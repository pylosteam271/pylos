#pragma once

#include "Board.h"

#include"Player.h"

class BoardStateChecker
{
public:
	enum class State
	{
		Win,
		OnGoing,
		SameColorSquare,
		Loser
	};


public:
	static State Check(const Board& board, const Player& player);
    static BoardStateChecker::State isSquare(const Board & board, const Board::Position & position);
	static bool CheckSquareUnderPosition(const Board & board, const Board::Position & position);

	static std::optional<Board::Square> GetDownRightSquare(const Board & board, const Board::Position & position);

private:
	static bool isLeft(const Board::Position& position);
	static bool isRight(const Board::Position& position);
	static bool isUp(const Board::Position& position);
	static bool isDown(const Board::Position& position);

	static std::optional<Board::Square> isUpLeftSquare(const Board & board, const Board::Position & position);
	static std::optional<Board::Square> isUpRightSquare(const Board & board, const Board::Position & position);
	static std::optional<Board::Square> isDownRightSquare(const Board & board, const Board::Position & position);
	static std::optional<Board::Square> isDownLeftSquare(const Board & board, const Board::Position & position);

	static bool isSameColorSquare(const Board & board, const Board::Square& square);

	static void printUnlockedPosition(const Board::Position & position);
};

