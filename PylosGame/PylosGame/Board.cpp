#include "Board.h"

const char kEmptyBoardCell[] = "(_)";



const std::optional<Piece>& Board::operator[](const Position & position) const
{
	const auto&[line, column, layer] = position;

	if (layer == 0)
	{
		if (line >= kSizeFirstLayer || column >= kSizeFirstLayer)
			throw std::out_of_range("Board index out of bound");
		return m_levelOne[line][column];
	}

	if (layer == 1)
	{
		if (line >= kSizeSecondLayer || column >= kSizeSecondLayer)
			throw std::out_of_range("Board index out of bound");
		return m_levelTwo[line][column];
	}

	if (layer == 2)
	{
		if (line >= kSizeThirdLayer || column >= kSizeThirdLayer)
			throw std::out_of_range("Board index out of bound");

		return m_levelThree[line][column];
	}

	throw "Invalid input";
}

std::optional<Piece>& Board::operator[](const Position& position)
{
	const auto&[line, column, layer] = position;

	if (layer == 0)
	{
		if (line >= kSizeFirstLayer || column >= kSizeFirstLayer)
			throw std::out_of_range("Board index out of bound");
		return m_levelOne[line][column];
	}

	if (layer == 1)
	{
		if (line >= kSizeSecondLayer || column >= kSizeSecondLayer)
			throw std::out_of_range("Board index out of bound");
		return m_levelTwo[line][column];
	}

	if (layer == 2)
	{
		if (line >= kSizeThirdLayer || column >= kSizeThirdLayer)
			throw std::out_of_range("Board index out of bound");

		return m_levelThree[line][column];
	}

	throw "Invalid input";
}

const size_t Board::GetkSizeFirstLayer()
{
	return kSizeFirstLayer;
}

const size_t Board::GetkSizeSecondLayer()
{
	return kSizeSecondLayer;
}

const size_t Board::GetkSizeThirdLayer()
{
	return kSizeThirdLayer;
}

const size_t Board::GetkSizeFourthLayer()
{
	return kSizeFourthLayer;
}

std::ostream & operator<<(std::ostream & os, const Board & board)
{
	Board::Position position;
	auto&[line, column, layer] = position;

	std::cout << std::endl;

	for (line = 0; line < Board::kSizeFirstLayer; ++line)
	{
		for (column = 0; column < Board::kSizeFirstLayer; ++column)
		{
			if (board.m_levelOne[line][column])
				os << "("<<*board.m_levelOne[line][column]<<")";
			else
				os << kEmptyBoardCell;
			os << ' ';
		}
		std::cout << std::endl;
	}

	std::cout << std::endl;


		for (line = 0; line < Board::kSizeSecondLayer; ++line)
		{
			for (column = 0; column < Board::kSizeSecondLayer; ++column)
			{
				if (board.m_levelTwo[line][column])
					os <<"("<< *board.m_levelTwo[line][column]<<")";
				else
					os << kEmptyBoardCell;
				os << ' ';
			}
			std::cout << std::endl;
		}
	

	std::cout << std::endl;


		for (line = 0; line < Board::kSizeThirdLayer; ++line)
		{
			for (column = 0; column < Board::kSizeThirdLayer; ++column)
			{
				if (board.m_levelThree[line][column])
					os <<"("<< *board.m_levelThree[line][column]<<")";
				else
					os << kEmptyBoardCell;
				os << ' ';
			}
			std::cout << std::endl;
		}


	std::cout << std::endl;

	if (board.m_levelFour.has_value())
		os <<"("<< *board.m_levelFour<<")";
	else
		os << kEmptyBoardCell;
	os << ' ';

	return os;
}

bool Board::IsFullFirstLayer() const
{
	Board board;
	for (int index = 0; index < board.m_levelOne.size(); index++)
	{
		if (std::all_of(
			board.m_levelOne[index].begin(),
			board.m_levelOne[index].end(),
			[](const std::optional<Piece>& optionalPiece) {
			return optionalPiece.has_value();
		}
		))
			continue;
		else
			return false;
	}

	return true;
}

bool Board::IsFullSecondLayer() const
{
	Board board;

	for (int index = 0; index < board.m_levelTwo.size(); index++)
	{
		if (std::all_of(
			board.m_levelTwo[index].begin(),
			board.m_levelTwo[index].end(),
			[](const std::optional<Piece>& optionalPiece) {
			return optionalPiece.has_value();
		}
		))
			continue;
		else
			return false;
	}

	return true;
}

bool Board::IsFullThirdLayer() const
{
	Board board;
	
	for (int index = 0; index < board.m_levelThree.size(); index++)
	{
		if (std::all_of(
			board.m_levelThree[index].begin(),
			board.m_levelThree[index].end(),
			[](const std::optional<Piece>& optionalPiece) {
			return optionalPiece.has_value();
		}
		))
			continue;
		else
			return false;
	}

	return true;
}

bool Board::IsFullForthLayer() const
{
	if (m_levelFour.has_value())
		return true;

	return false;
}


