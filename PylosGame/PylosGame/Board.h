#pragma once

#include "Piece.h"

#include<array>
#include<optional>


class Board
{
public:
	using Position = std::tuple<uint8_t, uint8_t, uint8_t>; //line, column, layer
	using Square = std::tuple<Board::Position, Board::Position, Board::Position, Board::Position>;


public:
	Board() = default;
	

	const std::optional<Piece>& operator [] (const Position& position) const;
	std::optional<Piece>& operator[](const Position& position);

	bool IsFullFirstLayer() const;
	bool IsFullSecondLayer() const;
	bool IsFullThirdLayer() const;
	bool IsFullForthLayer() const;


	friend std::ostream & operator<<(std::ostream & os, const Board & board);

public:
	static const size_t GetkSizeFirstLayer();
	static const size_t GetkSizeSecondLayer();
	static const size_t GetkSizeThirdLayer();
	static const size_t GetkSizeFourthLayer();

private:
	static const size_t kSizeFirstLayer = 4;
	static const size_t kSizeSecondLayer = 3;
	static const size_t kSizeThirdLayer = 2;
	static const size_t kSizeFourthLayer = 1;


private:
	std::array<std::array<std::optional<Piece>, kSizeFirstLayer>, kSizeFirstLayer> m_levelOne;
	std::array<std::array<std::optional<Piece>, kSizeSecondLayer>, kSizeSecondLayer> m_levelTwo;
	std::array<std::array<std::optional<Piece>, kSizeThirdLayer>, kSizeThirdLayer> m_levelThree;
	std::optional <Piece> m_levelFour;
};

