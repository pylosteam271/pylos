#include "Player.h"
#include <BoardStateChecker.h>

#include <utility>

#include <tuple>

const int kSizePieceVector = 15;

Player::Player(const std::string & name, Piece::Color color) :
	m_name(name)
{
	for (int index = 0; index < kSizePieceVector; index++)
	{
		Piece piece(color,false);
		m_pieces.push_back(piece);
	}
}

std::string Player::GetName()
{
	return m_name;
}

Piece::Color Player::GetColor()
{
	return m_pieces[0].GetColor();
}

Piece Player::PickPiece()
{
	if (m_pieces.empty())
		throw "There are no more pieces!";
	else
	{
		Piece piece = m_pieces.front();
		m_pieces.pop_back();
		return std::move(piece);
	}
}




Board::Position Player::PlacePiece(std::istream & is, Piece&& piece, Board & board)
{
	uint16_t line;
	uint16_t column;
	uint16_t layer;

	if(is>>line)
		if(is>>column)
			if (is >> layer)
			{
				Board::Position position = { static_cast<uint8_t>(line), static_cast<uint8_t>(column), static_cast<uint8_t>(layer) };
				auto& pieceToBePlaced = board[position];
				
				if (pieceToBePlaced)
					throw "That position is occupied by another piece! Choose another one!";

				if (layer == 0)
				{
					pieceToBePlaced = std::move(piece);
					return position;
				}

				else
				{
					if (BoardStateChecker::CheckSquareUnderPosition(board, position))
					{
						pieceToBePlaced = std::move(piece);
						ChangePiecesStatus(board, position);
						return position;
					}
				}

				
			}
	is.clear();
	is.seekg(std::ios::end);
	throw "That position is unavailable. Choose another one.";
}

Board::Position Player::MovePieceOnBoard(std::istream & is, Board & board)
{
	uint16_t fromLine, toLine;
	uint16_t fromColumn, toColumn;
	uint16_t fromLayer, toLayer;

	if (is >> fromLine)
		if (is >> fromColumn)
			if (is >> fromLayer)
			{
				Board::Position fromPosition = { static_cast<uint8_t>(fromLine), static_cast<uint8_t>(fromColumn), static_cast<uint8_t>(fromLayer) };
				auto& pieceToBeMoved = board[fromPosition];

				Piece piece = *board[fromPosition];

				if (!pieceToBeMoved)
					throw "There is no piece there!";

				if (piece.GetColor() != this->GetColor())
					throw "It`s not your piece. Choose another one.";

				if (piece.GetBlockedStatus())
					throw "This is blocked. You cannot take it. Choose another one.";


				std::cout << "Please, insert the position where you want to put your piece. (Enter: line, column, layer)\n";

				if (is >> toLine)
					if (is >> toColumn)
						if (is >> toLayer)
						{
							Board::Position toPosition = { static_cast<uint8_t>(toLine), static_cast<uint8_t>(toColumn), static_cast<uint8_t>(toLayer) };

							if (toLayer <= fromLayer)
								throw "You cannot move the piece on the same layer. Choose a higher layer!";

							auto& placePiece = board[toPosition];
							Piece piece = *board[toPosition];
							ChangePiecesStatus(board, toPosition);

							if (placePiece)
								throw "There is already a piece there.";

							placePiece = std::move(pieceToBeMoved);
							board[fromPosition].reset();
							return toPosition;
						}
				throw "Invalid enter. Please enter three values between 0 and 3";
			}
	is.clear();
	is.ignore();
	throw "Invalid enter. Please enter three values between 0 and 3";
}
Board::Position Player::TakePiecesFromBoard(std::istream & is, Board & board)
{
	uint16_t line;
	uint16_t column;
	uint16_t layer;
	if (is >> line)
	{
		if (is >> column)
		{
			if (is >> layer)
			{
				Board::Position position = { static_cast<uint8_t>(line), static_cast<uint8_t>(column), static_cast<uint8_t>(layer) };
				if (!board[position])
					throw "There is no piece there";

				Piece piece = *board[position];

				if (piece.GetBlockedStatus() == true)
					throw "This piece is blocked. You cannot take it";

				m_pieces.push_back(m_pieces.front());
				board[position].reset();

				return position;
			}
		}
	}
	is.clear();
	is.ignore();
	throw "Invalid enter. Please enter three values between 0 and 3";


}

void Player::ShowPieces()
{
	std::cout << " (" << GetNumberOfPieces() << ") : ";
	for (int index = 0; index < m_pieces.size(); index++)
		std::cout << static_cast<int>(m_pieces[index].GetColor()) << " ";
	std::cout << std::endl;
}

size_t Player::GetNumberOfPieces()
{
	return m_pieces.size();
}

bool Player::isOutOfPieces() const
{
	if (m_pieces.empty())
		return true;
	return false;
}

void Player::ChangePiecesStatus(Board & board, const Board::Position & position)
{
	Board::Square squareUnderPosition = *BoardStateChecker::GetDownRightSquare(board, position);
	
	board[std::get<0>(squareUnderPosition)]->SetBlockedStatus(true);
	board[std::get<1>(squareUnderPosition)]->SetBlockedStatus(true);
	board[std::get<2>(squareUnderPosition)]->SetBlockedStatus(true);
	board[std::get<3>(squareUnderPosition)]->SetBlockedStatus(true);
}

std::ostream & operator<<(std::ostream & os, Player & player)
{
	os << player.m_name;
	return os;
}
