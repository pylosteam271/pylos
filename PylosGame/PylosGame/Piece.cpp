#include "Piece.h"

#include <utility>

Piece::Piece():
	m_color(Color::None),
	m_isBlocked(false)
{
	//no code here
}

Piece::Piece(const Piece & other)
{
	*this = other;
}

Piece::Piece(Color color, bool isBlocked):
	m_color(color),
	m_isBlocked(isBlocked)
{
}

Piece::Piece(Piece && other)
{
	*this = std::move(other);
}

Piece & Piece::operator=(const Piece & other)
{
	m_color = other.m_color;
	m_isBlocked = other.m_isBlocked;
	return *this;
}

Piece & Piece::operator=(Piece && other)
{
	m_color = other.m_color;
	m_isBlocked = other.m_isBlocked;
	new(&other) Piece;

	return *this;
}

Piece::Color Piece::GetColor() const
{
	return m_color;
}

bool Piece::GetBlockedStatus()
{
	return m_isBlocked;
}

void Piece::SetBlockedStatus(bool status)
{
	this->m_isBlocked = status;
}

Piece::~Piece()
{
	m_color = Color::None;
}

std::ostream & operator<<(std::ostream & os, const Piece & piece)
{
	return os <<
		static_cast<int>(piece.m_color);
}
