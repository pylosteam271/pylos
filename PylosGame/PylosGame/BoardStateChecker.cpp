#include "BoardStateChecker.h"
#include<vector>

const int kFourthLayerIndex = 3;



bool BoardStateChecker::isLeft(const Board::Position& position)
{
	const auto&[line, column, layer] = position;

	size_t layerSize;

	if (layer == 0)
	{
		layerSize = Board::GetkSizeFirstLayer();
		if (column < layerSize  && column > 0)
			return true;
	}

	if (layer == 1)
	{
		layerSize = Board::GetkSizeSecondLayer();
		if (column < layerSize  && column > 0)
			return true;
	}

	if (layer == 2)
	{
		layerSize = Board::GetkSizeThirdLayer();
		if (column < layerSize  && column > 0)
			return true;
	}
	return false;
}

bool BoardStateChecker::isRight(const Board::Position& position)
{
	const auto&[line, column, layer] = position;

	size_t layerSize;

	if (layer == 0)
	{
		layerSize = Board::GetkSizeFirstLayer();
		if (column < layerSize - 1 && column >= 0)
			return true;
	}

	if (layer == 1)
	{
		layerSize = Board::GetkSizeSecondLayer();
		if (column < layerSize - 1 && column >= 0)
			return true;
	}

	if (layer == 2)
	{
		layerSize = Board::GetkSizeThirdLayer();
		if (column < layerSize - 1 && column >= 0)
			return true;
	}

	return false;
}

bool BoardStateChecker::isUp(const Board::Position& position)
{
	const auto&[line, column, layer] = position;

	size_t layerSize;

	if (layer == 0)
	{
		layerSize = Board::GetkSizeFirstLayer();
		if (line > 0 && line < layerSize)
			return true;
	}

	if (layer == 1)
	{
		layerSize = Board::GetkSizeSecondLayer();
		if (line > 0 && line < layerSize)
			return true;
	}

	if (layer == 2)
	{
		layerSize = Board::GetkSizeThirdLayer();
		if (line > 0 && line < layerSize)
			return true;
	}

	return false;
}

bool BoardStateChecker::isDown(const Board::Position& position)
{
	const auto&[line, column, layer] = position;

	size_t layerSize;

	if (layer == 0)
	{
		layerSize = Board::GetkSizeFirstLayer();
		if (line >= 0 && line < layerSize - 1)
			return true;
	}

	if (layer == 1)
	{
		layerSize = Board::GetkSizeSecondLayer();
		if (line >= 0 && line < layerSize - 1)
			return true;
	}

	if (layer == 2)
	{
		layerSize = Board::GetkSizeThirdLayer();
		if (line >= 0 && line < layerSize - 1)
			return true;
	}

	return false;
}

void BoardStateChecker::printUnlockedPosition(const Board::Position & position)
{
	const auto&[line, column, layer] = position;
	std::cout << "Hint! Position  " <<static_cast<uint16_t>(line) << ' ' 
		<< static_cast<uint16_t>(column) <<' '<< static_cast<uint16_t>(layer+1)<< " has been unlocked.\n";
}


std::optional<Board::Square> BoardStateChecker::isUpLeftSquare(const Board & board, const Board::Position & position) {

	const auto&[line, column, layer] = position;

	Board::Square square;


	if (isUp(position))
	{
		if (isLeft(position))
		{
			Board::Position up = { line - 1 , column , layer };
			Board::Position diagonal = { line - 1 , column - 1 , layer };
			Board::Position left = { line , column - 1 , layer };

			if (board[position].has_value() && board[up].has_value() 
				&& board[diagonal].has_value() && board[left].has_value())
			{
				square = { position, up, diagonal, left };
				printUnlockedPosition(diagonal);
				return square;
			}

		}

	}
	return {};
}

std::optional<Board::Square> BoardStateChecker::isUpRightSquare(const Board & board, const Board::Position & position)
{
	const auto&[line, column, layer] = position;

	Board::Square square;


	if (isUp(position))
	{
		if (isRight(position))
		{
			Board::Position up = { line - 1 , column , layer };
			Board::Position diagonal = { line - 1 , column + 1 , layer };
			Board::Position right = { line , column + 1 , layer };

			if (board[position].has_value() && board[up].has_value() 
				&& board[diagonal].has_value() && board[right].has_value())
			{
				square = { position, up, diagonal, right };
				printUnlockedPosition(up);
				return square;
			}

		}
	}
	return {};
}

std::optional<Board::Square> BoardStateChecker::isDownRightSquare(const Board & board, const Board::Position & position)
{
	const auto&[line, column, layer] = position;

	Board::Square square;


	if (isDown(position))
	{
		if (isRight(position))
		{
			Board::Position down = { line + 1 , column , layer };
			Board::Position diagonal = { line + 1 , column + 1 , layer };
			Board::Position right = { line , column + 1 , layer };

			if (board[position].has_value() && board[down].has_value() 
				&& board[diagonal].has_value() && board[right].has_value())
			{
				square = { position, down, diagonal, right };
				printUnlockedPosition(position);
				return square;
			}

		}
	}
	return {};
}

std::optional<Board::Square> BoardStateChecker::isDownLeftSquare(const Board & board, const Board::Position & position)
{
	const auto&[line, column, layer] = position;

	Board::Square square;


	if (isDown(position))
	{
		if (isLeft(position))
		{
			Board::Position down = { line + 1 , column , layer };
			Board::Position diagonal = { line + 1 , column - 1 , layer };
			Board::Position left = { line , column - 1 , layer };

			if (board[position].has_value() && board[down].has_value() 
				&& board[diagonal].has_value() && board[left].has_value())
			{
				square = { position, down, diagonal, left };
				printUnlockedPosition(diagonal);
				return square;
			}
		}
	}
	return {};
}


bool BoardStateChecker::isSameColorSquare(const Board & board, const Board::Square& square)
{
	const auto&[first, second, third, fourth] = square;

	std::vector<Piece> vector = { *board[first], *board[second], *board[third], *board[fourth] };

	return std::all_of(
		vector.begin(),
		vector.end(),
		[vector]
	(Piece piece)
	{
		return piece.GetColor() == vector[0].GetColor();
	});


	return false;
}


BoardStateChecker::State BoardStateChecker::isSquare(const Board & board, const Board::Position & position)
{
	const auto&[line, column, layer] = position;

	std::optional<Board::Square> square = isUpLeftSquare(board, position);

	State state = State::OnGoing;

	if (square.has_value() != NULL)
	{
		if (isSameColorSquare(board, *square))
			state = State::SameColorSquare;

	}

	square = isUpRightSquare(board, position);
	if (square.has_value() != NULL)
	{
		if (isSameColorSquare(board, *square))
			state = State::SameColorSquare;

	}

	square = isDownLeftSquare(board, position);
	if (square.has_value() != NULL)
	{
		if (isSameColorSquare(board, *square))
			state = State::SameColorSquare;
	}

	square = isDownRightSquare(board, position);
	if (square.has_value()!= NULL)
	{
		if (isSameColorSquare(board, *square))
			state = State::SameColorSquare;
	}


	return state;
}

bool BoardStateChecker::CheckSquareUnderPosition(const Board & board, const Board::Position & position)
{
	const auto&[line, column, layer] = position;

	Board::Position otherPosition = { line, column, layer - 1 };
	if (isDownRightSquare(board, otherPosition).has_value())
		return true;


	return false;
}

std::optional<Board::Square> BoardStateChecker::GetDownRightSquare(const Board & board, const Board::Position & position)
{
	const auto&[line, column, layer] = position;
	Board::Position otherPosition = { line, column, layer - 1 };
	return isDownRightSquare(board, otherPosition);
}


BoardStateChecker::State BoardStateChecker::Check(const Board & board, const Player& player)
{
	if (board.IsFullFirstLayer() && board.IsFullSecondLayer()
		&& board.IsFullThirdLayer() && board.IsFullForthLayer())
		return State::Win;

	else if (player.isOutOfPieces())
		return State::Loser;

	else return State::OnGoing;
}







