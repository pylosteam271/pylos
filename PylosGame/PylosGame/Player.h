#pragma once

#include "Piece.h"
#include "Board.h"

#include <string>
#include <vector>

class Player
{
public:
	Player(const std::string& name, Piece::Color color);

	std::string GetName();
	Piece::Color GetColor();

	//pick a ball
	Piece PickPiece();
	//Place piece on the board from the array
	Board::Position PlacePiece(std::istream& is,Piece&& piece, Board& board);

	//move piece from the board when it`s possible
	Board::Position MovePieceOnBoard(std::istream& is, Board& board);

	//take piece(s) back from the board when you can (returns he position from wich we take the piece
	Board::Position TakePiecesFromBoard(std::istream& is, Board& board);

	friend std::ostream& operator<<(std::ostream& os, Player& player);

	void ShowPieces(); //this will be deleted. It`s just for testing

	size_t GetNumberOfPieces();

	bool isOutOfPieces() const;

private:
	
	void ChangePiecesStatus(Board & board, const Board::Position & position);


private:
	std::string m_name;
	std::vector<Piece> m_pieces;
};

