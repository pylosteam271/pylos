#pragma once
#include"Board.h"
#include"Player.h"
#include"Piece.h"
#include"BoardStateChecker.h"

#include<iostream>
#include<string>
#include<fstream>
#include<algorithm>
#include<random>

class Game
{
public:

	void Introduction();
	void Run();

	void ReadRules();

private:

	void SameColorActions(Board& board, Player& player1);
};


