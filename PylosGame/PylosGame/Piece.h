#pragma once

#include<iostream>

class Piece
{
public:
public:
	enum class Color :uint8_t
	{
		Black, // = true
		White, // = true
		None
	};

public:
	Piece();
	Piece(const Piece& other);
	Piece(Color color, bool isBlocked);
	Piece(Piece&& other);

	Piece & operator=(const Piece& other);
	Piece & operator=(Piece&& other);


	Color GetColor() const;

	bool GetBlockedStatus();

	void SetBlockedStatus(bool status);



	~Piece();

	friend std::ostream & operator<<(std::ostream & os, const Piece & piece);

private:
	Color m_color;
	bool m_isBlocked;
};


