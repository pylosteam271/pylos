#include"Board.h"
#include "Player.h"
#include"Game.h"
#include "../Logging/Logging.h"

#include<iostream>
#include <fstream>


int main()
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	logger.log("The application has started", Logger::Level::Info);

	Game game;
	game.Introduction();

	return 0;
}