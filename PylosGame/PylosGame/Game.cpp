#include "Game.h"
#include"Board.h"
#include"BoardStateChecker.h"
#include"Piece.h"
#include"Player.h"

#include<algorithm>
#include<random>

void Game::Introduction()
{
	int action = 1;

	while (action)
	{
		std::cout << "\n\n\n\n";
		std::cout << "============ PYLOS GAME ===============" << std::endl;
		std::cout << std::endl << "\nEnter your choice(1-3):";
		std::cout << "\n\n1.Game Rules.\n2.Start Game\n3.End sesion.\n" << std::endl;
		std::cin >> action;

		switch (action)
		{
		case 1:
			std::cout << "============ PYLOS GAME RULES ===============";
			std::cout << std::endl << std::endl;

			Game game;

			game.ReadRules();

			continue;

		case 2:
			Game game2;
			game2.Run();
			break;
		case 3:
			exit(0);
		default: std::cout << "\nWrong choice!!";
			break;
		}
	}
}

void Game::Run()
{

	std::cout << "\nWelcome to the Pylos game!" << std::endl;
	std::array<Piece::Color, 2> vectorToShuffle = { Piece::Color::Black, Piece::Color::White };
	std::random_device rd;
	std::mt19937 g(rd());
	std::shuffle(vectorToShuffle.begin(), vectorToShuffle.end(), g);

	std::cout << "First player name:  " ;
	std::string name1;
	std::cin >> name1;


	Player player1(name1, vectorToShuffle[0]);

	std::cout << "Second player name:  " ;
	std::string name2;
	std::cin >> name2;

	Player player2(name2, vectorToShuffle[1]);

	if (static_cast<int>(player1.GetColor()) == 0)
	{
		std::swap(player1, player2);
	}


	for (int i = 0; i < vectorToShuffle.size(); i++)
	{
		std::string color;

		if (static_cast<int>(vectorToShuffle[i]) == 1)
		{
			color = "white";
			std::cout << player1.GetName() << ", you will have 15 \"" << color << "\" coloreed balls";
			std::cout << std::endl;
		}
		if (static_cast<int>(vectorToShuffle[i]) == 0)
		{
			color = "black";
			std::cout << player2.GetName() << ", you will have 15 \"" << color << "\" coloreed balls";
			std::cout << std::endl;
		}


	}
	std::cout << std::endl;
	std::cout << "Player with WHITE pieces, " << player1.GetName() << ", start!";
	std::cout << std::endl;

	std::cout << "\n\n\n\n";
	std::cout << "============ START ===============\n" << std::endl;

	Board board;
	std::cout << board;
	while (true)
	{
		int action = 1;

		while (action)
		{
			std::cout << "\n\n";
			std::cout << player1.GetName() << ", it`s your turn!\n\n";
			std::cout << "These are your pieces, " << player1.GetName() << "";
			player1.ShowPieces();
			std::cout << std::endl;
			std::cout << std::endl << "\nEnter your choice(1-2):";
			std::cout << "\n\n1.Place piece on board.\n2.Move piece from board" << std::endl;
			std::cin >> action;



			Board::Position position;
			BoardStateChecker::State currentState, winState;


			switch (action)
			{

			case 1:

				std::cout << "\nPlease, place a piece on the board! (Enter: line, column, layer)" << std::endl;

				while (true)
				{
					try
					{
						position = player1.PlacePiece(std::cin, player1.PickPiece(), board);
						break;
					}
					catch (const char* message)
					{
						std::cout << message << std::endl << std::endl;
						break;
					}
				}


				currentState = BoardStateChecker::isSquare(board, position);
				if (currentState == BoardStateChecker::State::SameColorSquare)
				{

					SameColorActions(board, player1);

				}

				std::cout << std::endl << board << std::endl;

				winState = BoardStateChecker::Check(board, player1);

				if ( winState == BoardStateChecker::State::Win)
				{
					std::cout << "Congratulation, " << player1.GetName() << ", you won. Game is over.\n";
					exit(0);
				}
				else if (winState == BoardStateChecker::State::Loser)
				{
					std::cout << player1.GetName() << ", you are out of pieces. You lost.\n";
					exit(0);
				}

				std::swap(player1, player2);
				continue;


			case 2:

				std::cout << std::endl;

				std::cout << "Please, choose a piece from the board! (Enter: line, column, layer)" << std::endl;
				while (true)
				{
					try
					{
						position = player1.MovePieceOnBoard(std::cin, board);
						std::cout << board<<std::endl;
						break;
					}
					catch (const char* message) {
						std::cout << message << std::endl << std::endl;
						std::swap(player1, player2);
						break;
					}
				}
				currentState = BoardStateChecker::isSquare(board, position);

				if (currentState == BoardStateChecker::State::SameColorSquare)
				{
					std::cout << std::endl << board << std::endl;

					SameColorActions(board, player1);

				}


				winState = BoardStateChecker::Check(board, player1);

				if (winState == BoardStateChecker::State::Win)
				{
					std::cout << "Congratulation, " << player1.GetName() << ", you won. Game is over.\n";
					exit(0);
				}
				else if (winState == BoardStateChecker::State::Loser)
				{
					std::cout << player1.GetName() << ", you are out of pieces. You lost.\n";
					exit(0);
				}


				std::swap(player1, player2);
				continue;

			default: std::cout << "\nWrong choice!!";
				break;
			}
		}

	}
}


void Game::ReadRules()
{
	std::string line;
	std::ifstream myfile("Rules.txt");
	if (myfile.is_open())
	{
		while (myfile.good())
		{
			getline(myfile, line);
			std::cout << line << std::endl;
		}
		myfile.close();
	}

	else std::cout << "Unable to open file";
}

void Game::SameColorActions(Board & board, Player& player1)
{
	while (true)
	{
		std::cout << std::endl << board << std::endl;
		std::cout << "How many pieces would you like to take back, " << player1.GetName() << "? 1 or 2" << std::endl;
		int piecesToTake;
		std::cin >> piecesToTake;
		std::cout << "Insert position/s of the piece/s.\n";
		if (piecesToTake == 1 || piecesToTake == 2)
		{
			for (int index = 0; index < piecesToTake; index++)
			{
				try
				{
					player1.TakePiecesFromBoard(std::cin, board);
				}
				catch (const char* message)
				{
					std::cout << message << std::endl << std::endl;
					index--;
					continue;
				}
			}
			break;
		}
		else
		{
			std::cout << "This is not a valid number! You can take 1 or 2 pieces!" << std::endl;
		}
	}
}

