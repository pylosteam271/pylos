#include "stdafx.h"
#include "CppUnitTest.h"


#include"Piece.h"
#include"Board.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PylosUnitTest
{
	TEST_CLASS(BoardTests)
	{
	public:

		TEST_METHOD(BoardConstructor)
		{
			Board board;
			Assert::IsFalse(board.IsFullFirstLayer() && board.IsFullSecondLayer() && board.IsFullThirdLayer());
			
		}

		TEST_METHOD(BoardIsFullFirstLAyer)
		{
			Board board;
			Assert::IsFalse(board.IsFullFirstLayer());
		}

		TEST_METHOD(BoardIsFullSecondLayer)
		{
			Board board;
			Assert::IsFalse(board.IsFullSecondLayer());
		}


		TEST_METHOD(BoardIsFullThirdLayer)
		{
			Board board;
			Assert::IsFalse(board.IsFullThirdLayer());
		}



		TEST_METHOD(BoardOperatorSquareBrakets)
		{
			Board board;
			Piece piece;
			Board::Position position = std::make_tuple<int>(0, 0, 0);
			bool ok;
			if (board[position] = std::move(piece))
			{
				ok = true;
			}
			else
			{
				ok = false;
			}

			Assert::IsTrue(ok);
		}

		TEST_METHOD(BoardDisplay)
		{
			Board board;
			std::cout << board;
		}

		TEST_METHOD(BoardSquareBreacketsExceptionThrown)
		{
			Board board;
			Board::Position position(std::make_tuple(100, 100, 7));
			auto boardException = [board, position]() {board[position]; };

			Assert::ExpectException<const char*>(boardException);
		}
	};
}