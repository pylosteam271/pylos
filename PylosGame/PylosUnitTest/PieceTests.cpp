#include "stdafx.h"
#include "CppUnitTest.h"

#include"Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PylosUnitTest
{		
	TEST_CLASS(PieceTests)
	{
	public:
		
		TEST_METHOD(PieceEmptyConstructor)
		{
			Piece piece;
			bool ok;
			if (piece.GetColor() == Piece::Color::None)
				ok = true;
			else
				ok = false;
				Assert::IsTrue(ok);
		}

		TEST_METHOD(CopyConstructor)
		{
			Piece piece(Piece::Color::Black, false);
			Piece anotherPiece;
			anotherPiece = piece;
			Assert::IsTrue(anotherPiece.GetColor() == Piece::Color::Black);
		}

		TEST_METHOD(ConstructorWithParameter)
		{
			Piece piece(Piece::Color::Black, false);
			Assert::IsTrue(piece.GetColor() == Piece::Color::Black);
		}

		TEST_METHOD(MoveConstructor)
		{
			Piece piece(Piece::Color::Black, false);
			Piece anotherPiece = std::move(piece);

			Assert::IsTrue(anotherPiece.GetColor() == Piece::Color::Black);
		}

		TEST_METHOD(IsWhitePiece)
		{
			Piece piece(Piece::Color::White, false);
			Assert::IsTrue(piece.GetColor() == Piece::Color::White);
		}

		TEST_METHOD(IsBlackPiece)
		{
			Piece piece(Piece::Color::Black, false);
			Assert::IsTrue(piece.GetColor() == Piece::Color::Black);
		}

		TEST_METHOD(IsNonePiece)
		{
			Piece piece(Piece::Color::None, false);
			Assert::IsTrue(piece.GetColor() == Piece::Color::None);
		}
	};
}