#include "stdafx.h"
#include "CppUnitTest.h"

#include"Player.h"
#include"Board.h"
#include"Piece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PylosUnitTest
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(PlayerConstructor)
		{
			Player player("Lidia", Piece::Color::Black);
			Assert::IsTrue(player.GetName() == "Lidia");
		}

		TEST_METHOD(PickPieceFunction)
		{
			Player player("Lidia", Piece::Color::Black);
			player.PickPiece();
			Assert::IsTrue(player.GetNumberOfPieces() == 14);
		}
	

		TEST_METHOD(ReturnNumberOfPieces)
		{
			Player player("Somebody", Piece::Color::Black);
			Assert::IsTrue(player.GetNumberOfPieces() == 15);

		}


	};
}