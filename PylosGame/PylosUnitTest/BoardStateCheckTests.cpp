#include "stdafx.h"
#include "CppUnitTest.h"

#include "BoardStateChecker.h"
#include "Board.h"
#include "Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PylosUnitTest
{
	TEST_CLASS(BoardStateCheckTests)
	{
		TEST_METHOD(IsSameColorSquareTest)
		{
			Board board;
			Piece piece1;
			Piece piece2;
			Piece piece3;
			Piece piece4;
			board[{0, 0, 0}] = std::move(piece1);
			board[{0, 1, 0}] = std::move(piece2);
			board[{1, 0, 0}] = std::move(piece3);
			board[{1, 1, 0}] = std::move(piece4);
			BoardStateChecker check;
			Assert::IsTrue(check.isSquare(board, std::make_tuple(1, 1, 0)) == BoardStateChecker::State::SameColorSquare);
		}

		TEST_METHOD(IsDifferentColorSquare)
		{
			Board board;
			Piece piece1(Piece::Color::Black, false);
			Piece piece2(Piece::Color::White, false);
			Piece piece3(Piece::Color::Black, false);
			Piece piece4(Piece::Color::White, false);
			board[{0, 0, 0}] = std::move(piece1);
			board[{0, 1, 0}] = std::move(piece2);
			board[{1, 0, 0}] = std::move(piece3);
			board[{1, 1, 0}] = std::move(piece4);
			BoardStateChecker check;
			Assert::IsFalse(check.isSquare(board, std::make_tuple(1, 1, 0)) == BoardStateChecker::State::SameColorSquare);
		}

		TEST_METHOD(IsNotSquare)
		{
			Board board;
			Piece piece1(Piece::Color::Black, false);
			board[{0, 0, 0}] = std::move(piece1);
			BoardStateChecker check;
			Assert::IsTrue(check.isSquare(board, std::make_tuple(0, 0, 0)) == BoardStateChecker::State::OnGoing);
		}
		
	
	};
}